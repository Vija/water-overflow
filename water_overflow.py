#!/usr/bin/env python3

import argparse
import sys
import os


def fill_water(capcity,total_volume):
	# 2D Matrix
	w, h = 100, 100;
	glasses = [[0 for x in range(w)] for y in range(h)]
	glasses[0][0] = total_volume
	level=0
	water_in = True
	while water_in:
		water_in = False
		j=0
		while j<=level:
			if glasses[level][j] > capcity:
				extraWater = float(glasses[level][j] - capcity)
				glasses[level][j] = capcity
				glasses[level+1][j] += extraWater / 2
				glasses[level+1][j+1] += extraWater / 2
				water_in = True
			j+=1
		level+=1
	actual_level = level-1
	
	for i in range(0,actual_level+1,1):
		line=""
		count=1
		row=[]
		for j in range(0,actual_level+1,1):
			row.append(float(glasses[i][j]))
			count+=1
		new_row=row[:(i+1)]
		
		numbers=" ".join(str(e) for e in new_row)
		print (('  '*((level)-i))+ ('{}'.format(numbers)))
		print(line)
		print(" ")

	return glasses,actual_level





def main():
	parser = argparse.ArgumentParser(description='This is a utility too calculate water overflow problem')
	parser.add_argument('-i','--ith_glass', type=int, help='position of ith glass', required=True)
	parser.add_argument('-j','--jth_glass', type=int, help='position of jth glass', required=True)
	parser.add_argument('-v','--volume', type=int, help='Volume to be measured in ml', required=True)
	parser.add_argument('-c','--capacity', default=250, help='Volume of glass in ml', required=False)
	args = vars(parser.parse_args())
	
	if len(sys.argv)==1:
	    parser.print_help(sys.stderr)
	    sys.exit(1)

	if  args['volume']>args['capacity']:
	
		glasses, level = fill_water(args['capacity'], total_volume=args['volume'])
		i=args['ith_glass']
		j=args['jth_glass']
		print(glasses[i][j])
	else:
		print("Ensure volume is greater than capacity")

if __name__ == '__main__':
    sys.exit(main())
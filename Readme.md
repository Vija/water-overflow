
# Water Overflow Calculator




#### Usage :

Volume in ml, default glass volume is set to 250(ml)
	
	$ git clone https://gitlab.com/Vija/water-overflow
	$ cd water-overflow
	$ python3 water_overflow.py -h


#### Example usage:
Finding capacity of glass where i=7 j=1 and volume  to be measured is 10 liters  
	
	$ python3 water_overflow.py -i 7 -j 1 -v 10000 


### Test:
	
	$ cd water-overflow
	$ pytest
 

### Prereqs:

	python3
	pytest
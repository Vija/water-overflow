#!/usr/bin/env python3
from water_overflow import fill_water


def test_fill_water():
	volume=10000
	glasses, level = fill_water(250,volume)

	total=0
	for i in range(0,level+1,1):
		for j in range(0,level+1,1):
			total=total+glasses[i][j]

	assert total == volume

